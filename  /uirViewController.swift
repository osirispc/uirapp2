//
//  uirViewController.swift
//  UIRnotes
//
//  Created by Osiris Ortiz on 4/24/19.
//  Copyright © 2019 Osiris Ortiz. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class uirViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var uirDetailView: UIView!
    @IBOutlet weak var individualNameTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var countyTextField: UITextField!
    @IBOutlet weak var dateOfIncidentTextField: UITextField!
    @IBOutlet weak var staffMakingNotificationTextField: UITextField!
    @IBOutlet weak var typeOfIncidentTextField: UITextField!
    @IBOutlet weak var investigatedByTextField: UITextField!
    @IBOutlet weak var jobTitleTextField: UITextField!
    @IBOutlet weak var antecedentsTextField: UITextView!
    @IBOutlet weak var contributingFactorsTextField: UITextView!
    @IBOutlet weak var responseTextField: UITextView!

    @IBOutlet weak var nocTextField: UITextField!
    @IBOutlet weak var nocTimeTextField: UITextField!
    @IBOutlet weak var mocTextField: UITextField!
    @IBOutlet weak var mocTimeTextField: UITextField!
    @IBOutlet weak var docTextField: UITextField!
    @IBOutlet weak var docTimeTextField: UITextField!
    @IBOutlet weak var countySaTextField: UITextField!
    @IBOutlet weak var countySaTimeTextField: UITextField!
    @IBOutlet weak var guardianTextField: UITextField!
    @IBOutlet weak var guardianTimeTextField: UITextField!
    @IBOutlet weak var muiLineTextField: UITextField!
    @IBOutlet weak var muiLineTimeTextField: UITextField!
    
    let kohoLogo = #imageLiteral(resourceName: "KohoLogo")
    
    let picker1 = UIPickerView()
    let picker2 = UIPickerView()
    let picker3 = UIPickerView()
    let picker4 = UIPickerView()
    let picker5 = UIPickerView()
    let picker6 = UIPickerView()
    let picker7 = UIPickerView()
    let picker8 = UIPickerView()
    
    //let datePicker1 = UIDatePicker ()
    
    var datePicker = UIDatePicker ()
    
    //let datePicker3 = UIDatePicker ()
    //let datePicker4 = UIDatePicker ()
    //let datePicker5 = UIDatePicker ()
    //let datePicker6 = UIDatePicker ()
    //let datePicker7 = UIDatePicker ()
    
    var formatter = DateFormatter()
    var toolBar = UIToolbar()

    var managedObjectContext: NSManagedObjectContext? {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    var uirNotesFetchResultsController: NSFetchRequest<UirNote>!
    var uirNotes = [UirNote]()
    var uirNote: UirNote?
    var isExsisting = false
    var indexPath: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker1.tag = 1
        picker2.tag = 2
        picker3.tag = 3
        picker4.tag = 4
        picker5.tag = 5
        picker6.tag = 6
        picker7.tag = 7
        picker8.tag = 8
        
        picker1.dataSource = self
        picker1.delegate = self
        picker2.dataSource = self
        picker2.delegate = self
        picker3.dataSource = self
        picker3.delegate = self
        picker4.dataSource = self
        picker4.delegate = self
        picker5.dataSource = self
        picker5.delegate = self
        picker6.dataSource = self
        picker6.delegate = self
        picker7.dataSource = self
        picker7.delegate = self
        picker8.dataSource = self
        picker8.delegate = self
        
        
        // MARK:  Turn Picker View On or OFF here
        
        locationTextField.inputView = picker1
        countyTextField.inputView = picker2
        typeOfIncidentTextField.inputView = picker3
        jobTitleTextField.inputView = picker4
        
        //nocTextField.inputView = picker5
        //mocTextField.inputView = picker6
        //docTextField.inputView = picker7
        
        muiLineTextField.inputView = picker8
        
        // MARK: DatePicker for the rest of the fields
        
        dateOfIncidentTextField.inputView = datePicker
        nocTimeTextField.inputView = datePicker
        mocTimeTextField.inputView = datePicker
        docTimeTextField.inputView = datePicker
        countySaTimeTextField.inputView = datePicker
        guardianTimeTextField.inputView = datePicker
        muiLineTimeTextField.inputView = datePicker
        locationTextField.inputView = picker1
        
        
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
        
        toolBar.setItems([doneButton], animated: true)
        
        dateOfIncidentTextField.inputAccessoryView = toolBar
        locationTextField.inputAccessoryView = toolBar
        nocTimeTextField.inputAccessoryView = toolBar
        mocTimeTextField.inputAccessoryView = toolBar
        docTimeTextField.inputAccessoryView = toolBar
        countySaTimeTextField.inputAccessoryView = toolBar
        guardianTimeTextField.inputAccessoryView = toolBar
        muiLineTimeTextField.inputAccessoryView = toolBar
        
        // Load data
        if let uirNote  = uirNote {
            individualNameTextField.text = uirNote.individualName
            locationTextField.text = uirNote.locationOfIncident
            countyTextField.text = uirNote.county
            dateOfIncidentTextField.text = uirNote.dateOfIncident
            staffMakingNotificationTextField.text = uirNote.staffMakingNotification
            typeOfIncidentTextField.text = uirNote.typeOfIncident
            investigatedByTextField.text = uirNote.investigatedBy
            jobTitleTextField.text = uirNote.jobTitle
            antecedentsTextField.text = uirNote.antecedents
            contributingFactorsTextField.text = uirNote.contributingFactors
            responseTextField.text = uirNote.response
            nocTextField.text = uirNote.noc
            nocTimeTextField.text = uirNote.nocTime
            mocTextField.text = uirNote.moc
            mocTimeTextField.text = uirNote.mocTime
            docTextField.text = uirNote.doc
            docTimeTextField.text = uirNote.docTime
            countySaTextField.text = uirNote.sa
            countySaTimeTextField.text = uirNote.saTime
            guardianTextField.text = uirNote.guardian
            guardianTimeTextField.text = uirNote.guardianTime
            muiLineTextField.text = uirNote.muiLine
            muiLineTimeTextField.text = uirNote.muiTime
            
        }
        
        if individualNameTextField.text != "" {
            isExsisting = true
        }
        
        // Delegate
        individualNameTextField.delegate = self
        locationTextField.delegate = self
        countyTextField.delegate = self
        dateOfIncidentTextField.delegate = self
        staffMakingNotificationTextField.delegate = self
        typeOfIncidentTextField.delegate = self
        investigatedByTextField.delegate = self
        jobTitleTextField.delegate = self
        antecedentsTextField.delegate = self
        contributingFactorsTextField.delegate = self
        responseTextField.delegate = self
        nocTextField.delegate = self
        nocTimeTextField.delegate = self
        mocTextField.delegate = self
        mocTimeTextField.delegate = self
        docTextField.delegate = self
        docTimeTextField.delegate = self
        countySaTextField.delegate = self
        countySaTimeTextField.delegate = self
        guardianTextField.delegate = self
        guardianTimeTextField.delegate = self
        muiLineTextField.delegate = self
        muiLineTimeTextField.delegate = self
        
      
        //This will add the Date Picker
        //showDatePicker1()
        //showDatePicker2()
        //showDatePicker3()
        //showDatePicker4()
        //showDatePicker5()
        //showDatePicker6()
       // showDatePicker7()
        
    }
    
    // MARK:  Set style for pickerView
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == dateOfIncidentTextField {
            datePicker.datePickerMode = .dateAndTime
        }
        if textField == nocTimeTextField {
            datePicker.datePickerMode = .dateAndTime
        }
        if textField == mocTimeTextField {
            datePicker.datePickerMode = .dateAndTime
        }
        if textField == docTimeTextField {
            datePicker.datePickerMode = .dateAndTime
        }
        if textField == countySaTimeTextField {
            datePicker.datePickerMode = .dateAndTime
        }
        if textField == guardianTimeTextField {
            datePicker.datePickerMode = .dateAndTime
        }
        if textField == muiLineTimeTextField {
            datePicker.datePickerMode = .dateAndTime
        }
    }
    
    // MARK: Set date format
    @objc func doneButtonTapped(textField: UITextField) {
        
        if dateOfIncidentTextField.isFirstResponder {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            dateOfIncidentTextField.text = formatter.string(from: datePicker.date)
        }
        
        if nocTimeTextField.isFirstResponder {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            nocTimeTextField.text = formatter.string(from: datePicker.date)
        }
        
        if mocTimeTextField.isFirstResponder {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            mocTimeTextField.text = formatter.string(from: datePicker.date)
        }
        
        if docTimeTextField.isFirstResponder {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            docTimeTextField.text = formatter.string(from: datePicker.date)
        }
        
        if countySaTimeTextField.isFirstResponder {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            countySaTimeTextField.text = formatter.string(from: datePicker.date)
        }
        
        if guardianTimeTextField.isFirstResponder {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            guardianTimeTextField.text = formatter.string(from: datePicker.date)
        }
        
        if muiLineTimeTextField.isFirstResponder {
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            muiLineTimeTextField.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    

    // MARK:  Save to Core Data
    
    func saveToCoreData(completion: @escaping () -> Void) {
        managedObjectContext!.perform {
            do {
                
                try self.managedObjectContext?.save()
                completion()
                print("Note saved to CoreData. ")
                
            }catch let error {
                print("Could not save to CoreData: \(error.localizedDescription)")
            }
        }
    }
    
    
    // Save  
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        if individualNameTextField.text == "" || individualNameTextField.text == "Individual Name" || locationTextField.text == "" || countyTextField.text == "" || dateOfIncidentTextField.text == "" || typeOfIncidentTextField.text == "" {
            
            let alertController = UIAlertController(title: "Missing Information", message:"You left one or more fields empty. Please make sure that all fields are filled before attempting to save.", preferredStyle: UIAlertController.Style.alert)
            let OKAction = UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil)
            
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        // Add Fields Here
        else {
            if (isExsisting == false) {
                let individualName = individualNameTextField.text
                let locationOfIncident = locationTextField.text
                let county = countyTextField.text
                let dateOfIncident = dateOfIncidentTextField.text
                let staffMakingNotification = staffMakingNotificationTextField.text
                let typeOfIncident = typeOfIncidentTextField.text
                let investigatedBy = investigatedByTextField.text
                let jobTitle = jobTitleTextField.text
                let antecedents = antecedentsTextField.text
                let contributingFactors = contributingFactorsTextField.text
                let response = responseTextField.text
                let noc = nocTextField.text
                let nocTime = nocTimeTextField.text
                let moc = mocTextField.text
                let mocTime = mocTimeTextField.text
                let doc = docTextField.text
                let docTime = docTimeTextField.text
                let sa = countySaTextField.text
                let saTime = countySaTimeTextField.text
                let guardian = guardianTextField.text
                let guardianTime = guardianTimeTextField.text
                let muiLine = muiLineTextField.text
                let muiLineTime = muiLineTimeTextField.text
                
                
                
                if let context = managedObjectContext {
                    let uirNote = UirNote(context: context)
                    
                    uirNote.individualName = individualName
                    uirNote.locationOfIncident = locationOfIncident
                    uirNote.county = county
                    uirNote.dateOfIncident = dateOfIncident
                    uirNote.staffMakingNotification = staffMakingNotification
                    uirNote.typeOfIncident = typeOfIncident
                    uirNote.investigatedBy = investigatedBy
                    uirNote.jobTitle = jobTitle
                    uirNote.antecedents = antecedents
                    uirNote.contributingFactors = contributingFactors
                    uirNote.response = response
                    uirNote.noc = noc
                    uirNote.nocTime = nocTime
                    uirNote.moc = moc
                    uirNote.mocTime = mocTime
                    uirNote.doc = doc
                    uirNote.docTime = docTime
                    uirNote.sa = sa
                    uirNote.saTime = saTime
                    uirNote.guardian = guardian
                    uirNote.guardianTime = guardianTime
                    uirNote.muiLine = muiLine
                    uirNote.muiTime = muiLineTime
                    
                    saveToCoreData() {
        
                        let isPresentingInAddFluidPatientMode = self.presentingViewController is UINavigationController
                        if isPresentingInAddFluidPatientMode {
                            self.dismiss(animated: true, completion: nil)
                        }else {
                            self.navigationController!.popViewController(animated: true)
                        }
                    }
                }
            }
                
            else if (isExsisting == true) {
                let uirNote = self.uirNote
                let managedObject = uirNote
                managedObject!.setValue(individualNameTextField.text, forKey: "individualName")
                managedObject!.setValue(locationTextField.text, forKey: "locationOfIncident")
                managedObject!.setValue(countyTextField.text, forKey: "county")
                managedObject!.setValue(dateOfIncidentTextField.text, forKey: "dateOfIncident")
                managedObject!.setValue(staffMakingNotificationTextField.text, forKey: "staffMakingNotification")
                managedObject!.setValue(typeOfIncidentTextField.text, forKey: "typeOfIncident")
                managedObject!.setValue(investigatedByTextField.text, forKey: "investigatedBy")
                managedObject!.setValue(jobTitleTextField.text, forKey: "jobTitle")
                managedObject!.setValue(antecedentsTextField.text, forKey: "antecedents")
                managedObject!.setValue(contributingFactorsTextField.text, forKey: "contributingFactors")
                managedObject!.setValue(responseTextField.text, forKey: "response")
                managedObject!.setValue(nocTextField.text, forKey: "noc")
                managedObject!.setValue(nocTimeTextField.text, forKey: "nocTime")
                managedObject!.setValue(mocTextField.text, forKey: "moc")
                managedObject!.setValue(mocTimeTextField.text, forKey: "mocTime")
                managedObject!.setValue(docTextField.text, forKey: "doc")
                managedObject!.setValue(docTimeTextField.text, forKey: "docTime")
                managedObject!.setValue(countySaTextField.text, forKey: "sa")
                managedObject!.setValue(countySaTimeTextField.text, forKey: "saTime")
                managedObject!.setValue(guardianTextField.text, forKey: "guardian")
                managedObject!.setValue(guardianTimeTextField.text, forKey: "guardianTime")
                managedObject!.setValue(muiLineTextField.text, forKey: "muiLine")
                managedObject!.setValue(muiLineTimeTextField.text, forKey: "muiTime")
                
                
              
                do {
                    try context.save()
                    let isPresentingInAddFluidPatientMode = self.presentingViewController is UINavigationController
                    if isPresentingInAddFluidPatientMode {
                        
                        
                        self.dismiss(animated: true, completion: nil)
                    }else {
                        self.navigationController!.popViewController(animated: true)
                    }
                }catch {
                    print("Failed to update existing note.")
                }
            }
        }
    }
    
    // Cancel
    
    @IBAction func cancel(_ sender: Any) {
        let isPresentingInAddFluidPatientMode = presentingViewController is UINavigationController
        
        if isPresentingInAddFluidPatientMode {
            dismiss(animated: true, completion: nil)
            
        }
            
        else {
            navigationController!.popViewController(animated: true)
            
        }
   
    }
    
    // Text field
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return false
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == "Individual Name") {
            textView.text = ""
        }
    }
    
    
    // MARK:  Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker1 {
            return location.count
        } else if pickerView == picker2 {
            return county.count
        } else if pickerView == picker3 {
            return type.count
        } else if pickerView == picker4 {
                return jobTitle.count
        //} else if pickerView == picker5 {
           // return noc.count
        } else if pickerView == picker6 {
            return moc.count
        } else if pickerView == picker7 {
            return doc.count
        } else if pickerView == picker8 {
            return muiLine.count
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == picker1 {
            return location [row]
        } else if pickerView == picker2 {
            return county [row]
        } else if pickerView == picker3 {
            return type [row]
        } else if pickerView == picker4 {
            return jobTitle [row]
       // } else if pickerView == picker5{
          //  return noc [row]
        } else if pickerView == picker6 {
            return moc [row]
        } else if pickerView == picker7 {
            return doc [row]
        } else if pickerView == picker8 {
            return muiLine [row]
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == picker1 {
            locationTextField.text = location[row]
            self.view.endEditing(false)
        } else if pickerView == picker2 {
            countyTextField.text = county[row]
            self.view.endEditing(false)
        } else if pickerView == picker3 {
            typeOfIncidentTextField.text = type[row]
            self.view.endEditing(false)
        } else if pickerView == picker4 {
            jobTitleTextField.text = jobTitle[row]
            self.view.endEditing(false)
        //} else if pickerView == picker5 {
            //nocTextField.text = noc[row]
            //self.view.endEditing(false)
        } else if pickerView == picker6 {
            mocTextField.text = moc[row]
            self.view.endEditing(false)
        } else if pickerView == picker7 {
            docTextField.text = doc[row]
            self.view.endEditing(false)
        } else if pickerView == picker8 {
            muiLineTextField.text = muiLine[row]
            self.view.endEditing(false)
        }
    }
    
    
    /*func showDatePicker1(){
        //Format the date here.
        datePicker1.datePickerMode = .dateAndTime
        //Set up Toolbar here.
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker1));
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton, spaceButton, cancelButton], animated: false)
        dateOfIncidentTextField.inputAccessoryView = toolbar
        dateOfIncidentTextField.inputView = datePicker1
    }
    
    @objc func donedatePicker1(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy h:mm a"
        dateOfIncidentTextField.text = formatter.string(from: datePicker1.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    */
    
    let county = [
        "",
        "Cuyahoga",
        "Geauga",
        "Lake",
        "Lorain",
        "Medina",
        "Summit",
        "WDC"]
    
    let location = [
        "",
        "Locust",
        "Lane Rd",
        "1200 Resource",
        "Mayfield",
        "Akins",
        "BECPAT",
        "Beeler",
        "Beresford",
        "BRASHI",
        "Brighton",
        "Broadview Hts.",
        "Brooklyn",
        "Brookpark",
        "Brookshire",
        "Bunts",
        "Chaucer",
        "Columbia Rd.",
        "Darrow Rd.",
        "DEMCHE",
        "Donovan",
        "Eastway",
        "Franklin",
        "Green1 Down",
        "Green2 Up",
        "Greencroft",
        "Greenhurst",
        "Harbin",
        "HARLOR",
        "Harwood",
        "Herrick Park",
        "Hilltop 51",
        "Hilltop 55",
        "Home Site",
        "Hunters Manor",
        "Hyde Park",
        "Independence",
        "Ira",
        "JACLAV",
        "KENDIA",
        "Kimberly Park",
        "Lamson",
        "Lane Road",
        "Leans",
        "Lee",
        "Locust",
        "McCracken",
        "Meadow Lane",
        "MONMAR",
        "Mowbray",
        "North Church Towers",
        "Northcliff",
        "NorthRidge",
        "Off-Site Storage",
        "Other",
        "Overbrook",
        "Overlook",
        "Regency",
        "Ridgeline",
        "Ridgewood",
        "S. Broadview",
        "Seven Hills",
        "Smith",
        "Solon",
        "Stearns",
        "Summit",
        "Sycamore",
        "The Way",
        "Transportation",
        "Valley Rd",
        "Van Aken",
        "Wadsworth",
        "Wallings",
        "Walter",
        "Wesley",
        "West 45th",
        "West Ridgewood Dr.",
        "WILMAT",
        "Wyleswood Drive",
        "York",
        
        ]
    let type = [
        "",
        "Aggression by Peer ",
        "Aggression toward peer ",
        "Aggression toward staff ",
        "Behavior ",
        "Dietary Problem ",
        "Elopement ",
        "Fall ",
        "Illness ",
        "Individual to ER/Urgent Care ",
        "Injury of Known causing not requiring medical attention ",
        "Injury of unknown origin not requiring medical attention ",
        "Med Refusal ",
        "Medication error ",
        "Missing money/Property ",
        "Out of Ratio ",
        "Property Destruction ",
        "Rights violation(no risk of harm) ",
        "Seizure ",
        "Self-Injury Behavior ",
        "Vehicle Accident ",
        "Other "
    ]
    
    let jobTitle = [
        "",
        "Supervisor",
        "Manager",
        "Director",
        "Nurse"
    ]
    
    let moc = [
        "",
        "Heidi Spaeth:  216-555-5555"
    ]
    
    
    let doc = ["", "Lina Workman - 555-5555" ]
    
    
    let noc = ["", "Stace Wolf - 555-5555" ]
    
    
    let muiLine = [
        "",
        "CCBDD 440-333-6884",
        "LCBDD MUI Line 440-329-3734 (8a-4p)",
        "LCBDD MUI Line 440-282-1131 (after hours)",
        "SCBDD MUI Line 330-634-8684 (8a-4p)",
        "SCBDD MUI Line 877-271-6733 (after hours)",
        "WDC Campus Support 216-285-9851 or 216-285-9852"
    ]
    
    @IBAction func send_Email(_sender: UIButton) {
        
        // Create CVS attachment
        
        let fileName = "UIR \(individualNameTextField.text!).csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        
        var csvText = "Individual Name, Location, County, Time of Incident, Staff Notification, Type of Incident, Description of Incident, Cause & Contributing Factors, What you did, NOC, NOC Time, MOC, MOC Time, DOC, DOC Time, County/SA, County/SA Time, Guardian, Guardian Time, MUI Line, MUI Line Time\n"
        
       
        let newLine = "\(individualNameTextField.text!), \(locationTextField.text!), \(countyTextField.text!), \(dateOfIncidentTextField.text!), \(staffMakingNotificationTextField.text!), \(typeOfIncidentTextField.text!),\(antecedentsTextField.text!), \(contributingFactorsTextField.text!), \(responseTextField.text!), \(nocTextField.text!), \(nocTimeTextField.text!), \(mocTextField.text!), \(mocTimeTextField.text!),\(docTextField.text!), \(docTimeTextField.text!), \(countySaTextField.text!), \(countySaTimeTextField.text!), \(guardianTextField.text!), \(guardianTimeTextField.text!), \(muiLineTextField.text!), \(muiLineTextField.text!) \n"
        
        csvText.append(contentsOf: newLine)
        
        
        do {
            try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
            
            //let vc = UIActivityViewController(activityItems: [path!], applicationActivities: [])
            
            //present(vc, animated: true, completion: nil)
            
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
        print(path ?? "not found")
        
        // MARK: Create Email
        
        if (MFMailComposeViewController.canSendMail()) {
            
            let image = UIImage(named: "KohoLogo")
            let imageData = image!.pngData() ?? nil
            let base64String = imageData?.base64EncodedString() ?? ""
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            
            mail.setSubject("UIR - \(individualNameTextField.text!) - \(dateOfIncidentTextField.text!)")
            mail.setToRecipients(["osirisortiz@msn.com", "osiris.ortiz@koinoniahomes.org"])
            mail.setMessageBody("<img src='data:image/png;base64, \(String(describing: base64String))' class='center'><h2>UIR Report</h2><br><b>Individual Name: </b>\(individualNameTextField.text!)\n\n<br><b>Site:</b> \(locationTextField.text!)\n\n<br><b>County:</b> \(countySaTextField.text!)\n\n<br><b>Time of Incident:</b> \(dateOfIncidentTextField.text!)\n\n<br><b>Staff Making Notification:</b> \(staffMakingNotificationTextField.text!)\n\n<br><b>Type of Incident:</b> \(typeOfIncidentTextField.text!)\n\n<br><br><h3>Brief Description of Incident: </h3><p>This UIR was investigated by \(investigatedByTextField.text!). \(antecedentsTextField.text!)</p>\n\n<br><br><h3>Cause and Contributing Factors: </h3><p>\(contributingFactorsTextField.text!)\n\n<br><br><h3>What you/staff did immediately to protect the individuals:</h3><p>\(responseTextField.text!)</p>\n\n<br><br><div><h3>Notifications (name and time):</h3><b>NOC: </b> \(nocTextField.text!) \(nocTimeTextField.text!)\n\n<br><b>MOC: </b>\(mocTextField.text!) \(mocTimeTextField.text!)\n\n<br><b>DOC: </b>\(docTextField.text!) \(docTimeTextField.text!)\n\n<br><b>County Liason/SA: </b> \(countySaTextField.text!) \(countySaTimeTextField.text!)\n\n<br><b>Guradian: </b>\(guardianTextField.text!) \(guardianTimeTextField.text!)\n\n<br><b>MUI Line: </b> \(muiLineTextField.text!) \(muiLineTimeTextField.text!)\n\n<br><p><b>Reported By: </b><br> \(investigatedByTextField.text!) \(jobTitleTextField.text!)</br></p>", isHTML: true)
            
             mail.addAttachmentData(NSData(contentsOf: path!)! as Data,  mimeType: "text/csv", fileName: "UIR - \(individualNameTextField.text!) - \(dateOfIncidentTextField.text!).csv")
            
            self.present(mail, animated: true, completion: nil)
                    } else {
            print("Email is not configured.  ")
        }
        
    }
        
        func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            switch result {
            case .cancelled:
                print("User cancelled")
                break
                
            case .saved:
                print("Mail is saved by user")
                break
                
            case .sent:
                print("Mail is sent successfully")
                break
                
            case .failed:
                print("Sending mail is failed")
                break
            default:
                break
            }
            
            controller.dismiss(animated: true)
            
        }
    
    
    var taskArr = [UirNote]()
    var task: UirNote!
    
    func creatCSV() -> Void {
        
        let fileName = "UIR \(individualNameTextField.text!).csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        var csvText = "Individual Name, Location, County, Time of Incident, Staff Notification, Type of Incident, Description of Incident, Cause & Contributing Factors, What you did, NOC, NOC Time, MOC, MOC Time, DOC, DOC Time, County/SA, County/SA Time, Guardian, Guardian Time, MUI Line, MUI Line Time\n"
        
        let newLine = "\(individualNameTextField.text!), \(locationTextField.text!), \(countyTextField.text!), \(dateOfIncidentTextField.text!), \(staffMakingNotificationTextField.text!), \(typeOfIncidentTextField.text!),\(antecedentsTextField.text!), \(contributingFactorsTextField.text!), \(responseTextField.text!), \(nocTextField.text!), \(nocTimeTextField.text!), \(mocTextField.text!), \(mocTimeTextField.text!),\(docTextField.text!), \(docTimeTextField.text!), \(countySaTextField.text!), \(countySaTimeTextField.text!), \(guardianTextField.text!), \(guardianTimeTextField.text!), \(muiLineTextField.text!), \(muiLineTextField.text!) \n"
        
        csvText.append(contentsOf: newLine)
        
        
        do {
            try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
            
            let vc = UIActivityViewController(activityItems: [path!], applicationActivities: [])
            
            present(vc, animated: true, completion: nil)
            
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
        print(path ?? "not found")
        
    }
    
    
    class ReadWriteText {
        
        var DocumentDirURL:URL {
            let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            return url
        }
        
        func fileURL(fileName: String, fileExtension: String) -> URL {
            return DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension(fileExtension)
        }
        
        func writeFile(writeString:String, to fileName:String, fileExtension:String = "txt") {
            let url = fileURL(fileName: fileName, fileExtension: fileExtension)
            
            do {
                try writeString.write(to: url, atomically: true, encoding: .utf8)
            } catch let error as NSError {
                print("Failed writing to URL:\(url), Error: " + error.localizedDescription)
            }
            
        }
        
        func readFile(from fileName:String, fileExtension:String = "txt") ->String {
            var readString = ""
            let url = fileURL(fileName: fileName, fileExtension: fileExtension)
            
            do {
                readString = try String(contentsOf: url)
            } catch let error as NSError {
                print("Failed reading to URL:\(url), Error: " + error.localizedDescription)
            }
            return readString
        }
    }
}


