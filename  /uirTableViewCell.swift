//
//  uirTableViewCell.swift
//  UIRnotes
//
//  Created by Osiris Ortiz on 4/24/19.
//  Copyright © 2019 Osiris Ortiz. All rights reserved.
//

import UIKit

class uirTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var dateOfIncidentLabel: UILabel!
    @IBOutlet weak var individualNameLabel: UILabel!
    @IBOutlet weak var typeOfIncidentLabel: UILabel!
    
    
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //shadowView.layer.shadowOffset = CGSize.zero
       //shadowView.layer.shadowColor = UIColor.darkGray.cgColor
        //shadowView.layer.shadowOpacity = 1
        //shadowView.layer.cornerRadius = 10
        
       shadowView.addShadowAndRoundedCorners()
       shadowView.backgroundColor = Theme.accent
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(uirNote: UirNote) {
        
        self.dateOfIncidentLabel.text = uirNote.dateOfIncident
        self.individualNameLabel.text = uirNote.individualName
        self.typeOfIncidentLabel.text = uirNote.typeOfIncident
        
    }
    
}

