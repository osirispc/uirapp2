//
//  uirListViewController.swift
//  UIRnotes
///Users/admin/Desktop/uirapp2/UIRnotes/Resources/Theme.swift
//  Created by Osiris Ortiz on 5/9/19.
//  Copyright © 2019 Osiris Ortiz. All rights reserved.
//

import UIKit
import CoreData

class uirListViewController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate {

    
    // MARK: Progerties
    
    @IBOutlet weak var addButton: AppUIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var uirViewController: uirViewController? = nil
    var uirNotes = [UirNote]()
    var filteredUirs = [UirNote]()
    let searchController = UISearchController(searchResultsController: nil)
    var filter: Bool = false
    
    var managedObjectContext: NSManagedObjectContext? {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
  
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        retrieveUirNotes()
        view.backgroundColor = Theme.backgroundColor
        addButton.createFloatingActionButton()
        searchBarSetup()
    }
    
    // Setup Search Controller
    func searchBarSetup() {
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search UIR's"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        
    }
    
    // Setup Scope Bar
    
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        retrieveUirNotes()
        
    }
    
    // MARK: Private Instance Methods
    
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    func isFiltering() -> Bool {
        
        print("Is filtering = true")
        return searchController.isActive && searchBarIsEmpty()
        
    }

}



extension uirListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
      // Later
        guard let searchText = searchController.searchBar.text else {return}
        
        if searchText == "" {
            retrieveUirNotes()
        }else{
            uirNotes = uirNotes.filter{
                $0.individualName!.contains(searchText)
            }
            tableView.reloadData()
        }
    }
    
    
}


extension uirListViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            print("filtered count")
            return filteredUirs.count
        }
            print("unfilted count")
        return uirNotes.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notesCell", for: indexPath) as! uirTableViewCell
        let uirNote: UirNote
        
        if isFiltering(){
            uirNote = filteredUirs[indexPath.row]
            print("Filtered Notes")
        }else{
            uirNote = uirNotes[indexPath.row]
            print("Unfiltered Notes")
        }
        
        
        // Configure the cell...
        //let uirNote: UirNote = uirNotes[indexPath.row]
        print("Show items")
        cell.configureCell(uirNote: uirNote)
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
        }
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            
            let uirNote = self.uirNotes[indexPath.row]
            context.delete(uirNote)
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            do {
                self.uirNotes = try context.fetch(UirNote.fetchRequest())
            }
                
            catch {
                print("Failed to delete note.")
            }
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
            
        }
        
        return [delete]
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Fetch Data
    
    func retrieveUirNotes() {
        managedObjectContext?.perform {
            
            self.fetchNotesFromCoreData { (uirNotes) in
                if let uirNotes = uirNotes {
                    self.uirNotes = uirNotes
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    
    func fetchNotesFromCoreData(completion: @escaping ([UirNote]?) -> Void) {
        managedObjectContext?.perform {
            var uirNotes = [UirNote]()
            let request: NSFetchRequest<UirNote> = UirNote.fetchRequest()
            
            // Code to Sort Data
            request.sortDescriptors = [NSSortDescriptor(key: "dateOfIncident", ascending: false)]
            
            
            do {
                uirNotes = try self.managedObjectContext!.fetch(request)
                completion(uirNotes)
            }catch{
                
            }
        }
    }
    
    
    
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailsSegue" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                
                
              //  let filteredUirs: UirNote
                
              //  if isFiltering
                
                let selectedNote: UirNote
                
                if isFiltering(){
                    
                    selectedNote = filteredUirs[indexPath.row]
                    print("Show filtered Details")
                }else{
                    selectedNote = uirNotes[indexPath.row]
                    print("Show Details")

                }
                
                
                
                
                
                
                let uirDetailsViewController = segue.destination as! uirViewController
                //let selectedNote: UirNote = uirNotes[indexPath.row]
                
                uirDetailsViewController.indexPath = indexPath.row
                uirDetailsViewController.isExsisting = false
                uirDetailsViewController.uirNote = selectedNote
                
            }
        }
            
        else if segue.identifier == "addUir" {
            print("User added a new note. ")
        }
    }
}
