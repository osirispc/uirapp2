//
//  UIViewExtensions.swift
//  Itinerary
//
//  Created by admin on 3/3/19.
//  Copyright © 2019 Osiris Ortiz. All rights reserved.
//

import UIKit

extension UIView {
    func addShadowAndRoundedCorners() {
        layer.shadowOpacity = 2
        layer.shadowOffset = CGSize(width: 0, height: 4)
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowRadius = 5
        layer.cornerRadius = 5
        
    }

}
