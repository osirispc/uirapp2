//
//  AppUIButton.swift
//  Itinerary
//
//  Created by Osiris Ortiz on 3/4/19.
//  Copyright © 2019 Osiris Ortiz. All rights reserved.
//

import UIKit

class AppUIButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Theme.tintColor
        layer.cornerRadius = frame.height / 2
        setTitleColor(UIColor.white, for: .normal)
        
        
        
    }

}
