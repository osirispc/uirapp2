//
//  UITextFieldExtension.swift
//  Itinerary
//
//  Created by admin on 3/10/19.
//  Copyright © 2019 Osiris Ortiz. All rights reserved.
//

import UIKit

extension UITextField {
    var hasValue: Bool {
        guard text == "" else { return true }
        
        
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
            imageView.image = #imageLiteral(resourceName: "warning")
            imageView.contentMode = .scaleAspectFit
            
            rightView = imageView
            rightViewMode = .unlessEditing
        
        return false
    }
}
