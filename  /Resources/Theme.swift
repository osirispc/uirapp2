//
//  Theme.swift
//  Itinerary
//
//  Created by admin on 3/3/19.
//  Copyright © 2019 Osiris Ortiz. All rights reserved.
//

import UIKit

class Theme {
    static let mainFontName = "Papyrus"
    static let bodyFontName = "AvenirNext-Regular"
    static let bodyFontNameBold = "AvenirNext-Bold"
    static let bodyFontNameDemiBold = "AvenirNext-DemiBold"
    static let accent = UIColor(named: "Accent")
    static let backgroundColor = UIColor(named: "Background")
    static let tintColor = UIColor(named: "Tint")
}
